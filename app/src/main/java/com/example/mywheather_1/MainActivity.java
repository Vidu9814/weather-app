package com.example.mywheather_1;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String API_KEY = "d50b92e4255e2a1e6cb061c2001fddd2";

    Button BtnSearch;
    EditText CityName;
    ImageView IconWeather;
    TextView TempC, Location;
    ListView DailyWeather;

    private List<Weather> weatherList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BtnSearch = findViewById(R.id.BtnSearch);
        CityName = findViewById(R.id.CityName);
        IconWeather = findViewById(R.id.IconWeather);
        TempC = findViewById(R.id.TempC);
        Location = findViewById(R.id.Location);
        DailyWeather = findViewById(R.id.DailyWeather);

        BtnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String city = CityName.getText().toString();
                if (city.isEmpty())
                    Toast.makeText(MainActivity.this, "Please Enter Your City", Toast.LENGTH_SHORT).show();
                else {
                    loadWeatherByCityName(city);
                }
            }
        });

        DailyWeather.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               // Weather weather = weatherList.get(position);
                Intent intent = new Intent(MainActivity.this,DataShowActivity.class);
              //  intent.putExtra("date",weather.getDate());
              //  intent.putExtra("icon",weather.getIcon());
              //  intent.putExtra("temp",weather.getTemp());
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.select_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()){

            case R.id.item1:
                Intent intent = new Intent(MainActivity.this,DataShowActivity.class);
                startActivity(intent);
                return true;

            case R.id.item3:
                Intent abc = new Intent(MainActivity.this,SettingsActivity.class);
                startActivity(abc);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void loadWeatherByCityName(String city) {
        String apiUrl = "https://api.openweathermap.org/data/2.5/weather?q="+city+"&&units=metric&appid="+API_KEY;
        Ion.with(this)
                .load(apiUrl)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {

                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (e != null){
                            e.printStackTrace();
                            Toast.makeText(MainActivity.this, "", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            JsonObject main = result.get("main").getAsJsonObject();
                            double temp = main.get("temp").getAsDouble();
                            TempC.setText(temp+"°C");

                            JsonObject sys = result.get("sys").getAsJsonObject();
                            String country = sys.get("country").getAsString();
                            Location.setText(city+", "+country);

                            JsonArray weather = result.get("weather").getAsJsonArray();
                            String icon = weather.get(0).getAsJsonObject().get("icon").getAsString();
                            loadIcon(icon);

                            JsonObject coord = result.get("coord").getAsJsonObject();
                            double lon = coord.get("lon").getAsDouble();
                            double lat = coord.get("lat").getAsDouble();
                            loadDailyForecast(lon, lat);

                        }
                    }
                });
    }

    private void loadDailyForecast(double lon, double lat) {
        String apiUrl = "https://api.openweathermap.org/data/2.5/forecast?lat="+lat+"&lon="+lon+"&&units=metric&appid="+ API_KEY;
        Ion.with(this)
                .load(apiUrl)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {

                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (e != null){
                            e.printStackTrace();
                            Toast.makeText(MainActivity.this, "", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            List<Weather> weatherList = new ArrayList<>();
                            JsonArray list = result.get("list").getAsJsonArray();
                            for (int i=1;i<list.size();i++){
                                Long date = list.get(i).getAsJsonObject().get("dt").getAsLong();
                                Double temp = list.get(i).getAsJsonObject().get("main").getAsJsonObject().get("temp").getAsDouble();
                                String icon = list.get(i).getAsJsonObject().get("weather").getAsJsonArray().get(0).getAsJsonObject().get("icon").getAsString();
                                weatherList.add(new Weather(date, temp, icon));

                            }
                            DailyWeatherAdaptor dailyWeatherAdaptor = new DailyWeatherAdaptor(MainActivity.this, weatherList);
                            DailyWeather.setAdapter(dailyWeatherAdaptor);

                        }
                    }
                });

     }

    private void loadIcon(String icon) {
        Ion.with(this)
                .load("https://openweathermap.org/img/w/"+icon+".png")
                .intoImageView(IconWeather);
    }
}