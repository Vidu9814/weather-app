package com.example.mywheather_1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.koushikdutta.ion.Ion;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class DailyWeatherAdaptor extends ArrayAdapter<Weather> {

    private Context context;
    private List<Weather> weatherList;

    public DailyWeatherAdaptor(@NonNull Context context, @NonNull List<Weather> weatherList) {
        super(context, 0, weatherList);
        this.context = context;
        this.weatherList = weatherList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.item_weather, parent, false);

        TextView nDate = convertView.findViewById(R.id.nDate);
        TextView TempC = convertView.findViewById(R.id.TempC);
        ImageView IconWeather = convertView.findViewById(R.id.IconWeather );

        Weather weather = weatherList.get(position);
        TempC.setText(weather.getTemp()+ " °C");

        Ion.with(context)
                .load("https://openweathermap.org/img/w/"+weather.getIcon()+".png")
                .intoImageView(IconWeather);
        Date date = new Date(weather.getDate()*1000);
        DateFormat dateFormat = new SimpleDateFormat("EEE, MMM yy", Locale.ENGLISH);
        nDate.setText(dateFormat.format(date));

        return convertView;
    }
}
