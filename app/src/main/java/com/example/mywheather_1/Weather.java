package com.example.mywheather_1;

public class Weather {

    private  Long date;
    private Double temp;
    private String icon;

    public long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public double getTemp() {
        return temp;
    }

    public void setTemp(Double temp) {
        this.temp = temp;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Weather(Double temp, String icon){
        this.temp = temp;
        this.icon = icon;
    }

    public Weather(Long Date, Double temp, String icon){
        this.date = Date;
        this.icon = icon;
        this.temp = temp;
    }

}
